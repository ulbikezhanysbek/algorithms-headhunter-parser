## Project
To see all files of the scraping go to path: <br/>
_algorithms_headhunter_parser/parser/_
## Settings
Make sure that you installed **docker-compose** and **Pycharm**. <br/>
_In order to install docker-compose to Ubuntu use this command:_
```
$ sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```
Open the Project in PyCharm and up the project by docker-compose using this command:
```
$ docker-compose -f local.yml up --build
```
In order to tune the Project Interpreter do steps below:
* Open the Window of Project Interpreter: PyCharm > Preferences > Project: algorithms-headhunter-parser > Python Interpreter
* Add Project Interpreter:
![picture](project_interp1.jpeg)

* In the window which appeared choose Docker as a Server, as a Configuration file "local.yml" and for Service choose Django.
![picture](project_interpreter2.jpeg)

* Click the button ok.

### Setting Up Your Users <br/>
Django server will be run in this server: http://0.0.0.0:9000/admin/   <br/>
Before that to create an **superuser account**, use this command::
```
$ docker-compose -f local.yml run --rm python manage.py createsuperuser
```
### Migrations
If you make some changes in models of project run these commands to make migrations:
```
docker-compose -f local.yml run --rm django python manage.py makemigrations parser
docker-compose -f local.yml run --rm django python manage.py migrate parser
```

### Run scraper 
In order to get all links of adverts and put it to redisQueue, in scraper.py file call main() function of adver_links.py:
![picture](run_scraper.jpeg)

Then run this command:
```
docker-compose -f local.yml run --rm django python manage.py scraper
```
In order to scrape all data from the links of adverts and put it to database of Django change file in scraper.py file:
![picture](run_scr2.jpeg)

Then run this command:
``` 
docker-compose -f local.yml run --rm django python manage.py scraper
```
### Export to csv file
To export the data to csv file run this command:
``` 
docker-compose -f local.yml run --rm django python manage.py export
```
### Clean containers
To clean containers of Docker run this command:
``` 
docker-compose -f local.yml down -v
```
###  Down project
To down the project use this command:
``` 
docker-compose -f local.yml down
```
