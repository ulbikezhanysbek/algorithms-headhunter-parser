import os
import parser
import sys

from bs4 import BeautifulSoup
from algorithms_headhunter_parser.parser.proxy import proxy, useragent, change_proxy
from algorithms_headhunter_parser.parser.redisqueue import RedisQueue
from celery.schedules import crontab
from celery.task import periodic_task
from django.utils import timezone
from celery import shared_task
from celery import Celery

base_url = "https://hh.kz/catalog/"
gen_proxy = proxy()
gen_useragent = useragent()


def get_jobs_advert_links(url_of_page, rediss):
    print("IN function")
    url = os.path.join(base_url, url_of_page)
    # changes IP
    page = change_proxy(url, gen_proxy, gen_useragent)
    redis = RedisQueue(rediss, host='redis', port=6379)
    soup2 = BeautifulSoup(page.content, "html.parser")
    # gets all a classes with pages
    page_number_soup = soup2.find_all("a", {"class": "bloko-button HH-Pager-Control"})
    # gets page number
    if page_number_soup:
        page_number = int(page_number_soup[-1].get_text())
    else:
        page_number = 1
    for page_index in range(page_number):
        page = {'page': page_index}
        page2 = change_proxy(url, gen_proxy, gen_useragent, params=page)
        soup = BeautifulSoup(page2.content, "html.parser")
        # finds all advert_links
        adverts_soup = soup.find_all("a", {"class": "bloko-link HH-LinkModifier"})
        for advert_index in range(len(adverts_soup)):
            advert_link = adverts_soup[advert_index]["href"]
            redis.put(advert_link)
        sys.stdout.write("STEP: ")
        print(page_index + 1)

    sys.stdout.write("\nI ended with putting advert links to redis and it was much more easier than I thought!!!\n")


#
# @periodic_task(run_every=crontab(minute='*/2'))
# def periodic_just_for_lulz():
#     now = timezone.now()
@periodic_task(run_every=crontab(hour="*/12"))
def main():
    print(9)
    now = timezone.now()
    urls = [#'Avtomobilnyj-biznes/', 'Administrativnyj-personal/', 'Banki-Investicii-Lizing/', 'Bezopasnost/',
            #'Buhgalterija-Upravlencheskij-uchet-Finansy-predprijatija/', 'Vysshij-menedzhment/',
            #'Gosudarstvennaja-sluzhba-nekommercheskie-organizacii/', 'Marketing-Reklama-PR/', 'Medicina-Farmacevtika/',
            #'Nauka-Obrazovanie/', 'Nachalo-karery-Studenty/', 'Prodazhi/', 'Proizvodstvo/', 'Rabochij-personal/',
            #'Dobycha-Syrja/', 'Domashnij-personal/', 'Zakupki/', 'Installjacija-i-servis/',
            'Informacionnye-tehnologii-Internet-Telekom/' ]#'Iskusstvo-Razvlechenija-Mass-media/', 'Konsultirovanie/',
            #'Sportivnye-kluby-fitnes-salony-krasoty/', 'Strahovanie', 'Stroitelstvo-Nedvizhimost/',
            #'Transport-Logistika/', 'Turizm-Gostinicy-Restorany/', 'Upravlenie-personalom-Treningi/', 'Juristy/']


    redis = [#'Avtomobilnyj-biznes', 'Administrativnyj-personal', 'Banki-Investicii-Lizing', '''Bezopasnost''',
             #'Buhgalterija-Upravlencheskij-uchet-Finansy-predprijatija', 'Vysshij-menedzhment',
             #'Gosudarstvennaja-sluzhba-nekommercheskie-organizacii', 'Marketing-Reklama-PR', 'Medicina-Farmacevtika',

             #'Nauka-Obrazovanie', 'Nachalo-karery-Studenty', 'Prodazhi', 'Proizvodstvo', 'Rabochij-personal',
             #'Dobycha-Syrja', 'Domashnij-personal', 'Zakupki', 'Installjacija-i-servis',
             'Informacionnye-tehnologii-Internet-Telekom'] #'Iskusstvo-Razvlechenija-Mass-media', 'Konsultirovanie',
             #'Sportivnye-kluby-fitnes-salony-krasoty', 'Strahovanie', 'Stroitelstvo-Nedvizhimost',
             #'Transport-Logistika', 'Turizm-Gostinicy-Restorany', 'Upravlenie-personalom-Treningi', 'Juristy']

    for i in range(len(urls)):
        get_jobs_advert_links(urls[i], redis[i])
        print(f"Time is -------{now}")
if __name__ == "__main__":
    main()
