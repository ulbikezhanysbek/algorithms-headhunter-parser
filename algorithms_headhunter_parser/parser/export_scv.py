from djqscsv import write_csv
from algorithms_headhunter_parser.parser.models import Job


def export():
    qs = Job.objects.all()
    with open('jobs.csv', 'wb') as csv_file:
        write_csv(qs, csv_file, encoding="utf-8")

    csv_file.close()
