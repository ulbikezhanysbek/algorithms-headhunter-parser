import requests


def proxy():
    proxies = open('algorithms_headhunter_parser/parser/ip/proxies.txt').read().split('\n')
    while True:
        for proxy in proxies:
            yield proxy


def useragent():
    useragents = open('algorithms_headhunter_parser/parser/ip/useragents.txt').read().split('\n')
    while True:
        for useragent in useragents:
            yield useragent


def change_proxy(url, gen_proxy, gen_useragent, params={}):
    check = True

    while True:
        if not check:
            break

        _proxy = {'http': 'http://' + next(gen_proxy)}
        _useragent = {"X-Requested-With": "XMLHttpRequest", 'User-Agent': next(gen_useragent)}

        try:
            r = requests.get(url, params=params, headers=_useragent, proxies=_proxy, timeout=3)
            check = False
        except:
            continue

    return r
