# from requests import Response
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from algorithms_headhunter_parser.parser.models import Job
from algorithms_headhunter_parser.parser.serializers import ResultSerializer
from rest_framework import status, generics
from rest_framework.views import APIView


class ResultView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        url = request.data.get("url")
        advert = Job.objects.filter(urls__contains=url)
        if advert:
            serializer = ResultSerializer(advert, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({
            'error': 'There does not exist a such link !'
        })
