from django.contrib import admin
from algorithms_headhunter_parser.parser.models import Job

admin.site.register(Job)
