from rest_framework import serializers
from algorithms_headhunter_parser.parser.models import Job
from algorithms_headhunter_parser.utils.utils import ChoiceValueDisplayField


class ResultSerializer(serializers.ModelSerializer):
    category = ChoiceValueDisplayField()
    class Meta:
        model = Job
        fields = '__all__'
