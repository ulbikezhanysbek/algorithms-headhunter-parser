from django.apps import AppConfig

services = None


class ParserConfig(AppConfig):
    name = 'algorithms_headhunter_parser.parser'
