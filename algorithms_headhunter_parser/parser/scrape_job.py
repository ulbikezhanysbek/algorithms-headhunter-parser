import os
import json
from bs4 import BeautifulSoup
from .redisqueue import RedisQueue
from .proxy import change_proxy, proxy, useragent
from algorithms_headhunter_parser.parser.models import Job
from celery.schedules import crontab
from celery.task import periodic_task
from django.utils import timezone
import sys


class MainScrape:
    print("MainScrape class")

    def __init__(self, redis):
        self.proxy = proxy()
        self.useragent = useragent()
        self.redis = redis

    # Take all info about job
    def scrape_page(self, link, category):
        sys.stdout.write("I am heeereeeeeeee-----------------")
        # link = "a/show/{}".format(id)
        url = link
        request = change_proxy(url, self.proxy, self.useragent)
        soup = BeautifulSoup(request.content, "html.parser")
        sys.stdout.write("Url:  ")
        sys.stdout.write(url)

        #get position
        position = soup.find("h1", {"class": "bloko-header-1"})
        if position:
            position = position.get_text().strip()

        #get salary
        salary_from = None
        salary_to = None
        salary = soup.find("p", {"class", "vacancy-salary"})
        if salary:
            salary = (salary.get_text()).replace(u'\xa0', '')
            try:
                arr = [int(s) for s in salary.split() if s.isdigit()]
                if len(arr) == 2:
                    salary_from = arr[0]
                    salary_to = arr[1]
                elif "от" in salary:
                    salary_from = arr[0]
                else:
                    salary_to = arr[0]
            except Exception as e:
                print("Error od salary is: ", e)
                salary_from = None
                salary_to = None

        # get company name
        company_name = soup.find("span", {"class":"bloko-section-header-2 bloko-section-header-2_lite"})
        if company_name:
            company_name = (company_name.get_text()).replace(u'\xa0', ' ')

        # get address
        city = soup.find("div", {"class", "vacancy-company vacancy-company_with-logo"})
        if city:
            city = city.find("p")
            if city:
                city = city.get_text()

        # get experience
        experience = soup.find("span", {"data-qa": "vacancy-experience"})
        if experience:
            experience = experience.get_text()

        # get schedule
        schedule = soup.find("p", {"data-qa": "vacancy-view-employment-mode"})
        if schedule:
            schedule = schedule.get_text()

        # job description
        job_description = soup.find("div", {"class": "g-user-content"})
        if job_description:
            job_description = job_description.get_text().strip()

        # get published date
        published_date = soup.find("p", {"class": "vacancy-creation-time"})
        if published_date:
            published_date = published_date.get_text().strip().replace(u'\xa0', ' ')


        #Put data into db
        sys.stdout.write("I ended to get data from the site, now i will put it to database")
        if(Job.objects.filter(url=url)).exists():
            sys.stdout.write("It exists in db!")
        else:
            sys.stdout.write("It doesn't exist in db")
            job = Job(position=position, url=url, category=category, salary_from=salary_from, salary_to=salary_to, company_name=company_name, city=city,
                      experience=experience, schedule=schedule, job_description=job_description,
                      published_date=published_date)
            job.save()


@periodic_task(run_every=crontab(hour='*/13'))
def main():
    print("Print from Scrape main")
    redis_list = ['Avtomobilnyj-biznes', 'Administrativnyj-personal', 'Banki-Investicii-Lizing', '''Bezopasnost''',
             'Buhgalterija-Upravlencheskij-uchet-Finansy-predprijatija', 'Vysshij-menedzhment',
             'Gosudarstvennaja-sluzhba-nekommercheskie-organizacii', 'Marketing-Reklama-PR', 'Medicina-Farmacevtika',
             'Nauka-Obrazovanie', 'Nachalo-karery-Studenty', 'Prodazhi', 'Proizvodstvo', 'Rabochij-personal',
             'Dobycha-Syrja', 'Domashnij-personal', 'Zakupki', 'Installjacija-i-servis',
             'Informacionnye-tehnologii-Internet-Telekom', 'Iskusstvo-Razvlechenija-Mass-media', 'Konsultirovanie',
             'Sportivnye-kluby-fitnes-salony-krasoty', 'Strahovanie', 'Stroitelstvo-Nedvizhimost',
             'Transport-Logistika', 'Turizm-Gostinicy-Restorany', 'Upravlenie-personalom-Treningi', 'Juristy']

    for i in range(len(redis_list)):
        q = RedisQueue(redis_list[i], host='redis', port=6379)
        eq = RedisQueue('err_urls', host='redis', port=6379)
        ms = MainScrape(redis_list[i])

        # get links from redis
        while q.qsize() > 0:
            link = q.get()
            try:
                ms.scrape_page(link.decode("utf-8"), redis_list[i])
                sys.stdout.write("---------Ended 1 page-----------")
            except Exception as e:
                sys.stdout.write("*-*-*-*-*-*-*- ERR: ")
                sys.stdout.write(str(e))
                eq.put(link)

        #to clean a redis
        while q.qsize() > 0:
            clean_redis = q.get()
            # sys.stdout.write(clean_redis.qsize())
    sys.stdout.write("I ended with parsing and putting it to database")
    # sys.stdout.write(clean_redis.qsize())
    sys.stdout.write("I ended with parsing and putting it to database")


if __name__ == "__main__":
    main()
