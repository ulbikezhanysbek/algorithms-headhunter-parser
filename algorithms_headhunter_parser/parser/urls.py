from django.urls import path
from algorithms_headhunter_parser.parser import views

app_name = 'parser'

urlpatterns = [
    path('advert-list/', views.ResultView.as_view()),
]
