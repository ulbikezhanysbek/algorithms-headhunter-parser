from django.core.management.base import BaseCommand
import sys
from algorithms_headhunter_parser.parser.scrape_job import main


class Command(BaseCommand):

    def handle(self, *args, **options):
        sys.stdout.write("Main function is starting!\n")
        main()
        sys.stdout.write("Main function ended!\n")
