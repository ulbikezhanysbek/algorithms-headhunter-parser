from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db.models import URLField


class Job(models.Model):

    position = models.CharField(max_length=350, null=True, blank=True)
    url = models.URLField(verbose_name="Url", null=True, blank=True)
    category = models.CharField(max_length=2000, null=True, blank=True)
    salary_from = models.IntegerField(null=True, blank=True)
    salary_to = models.IntegerField(null=True, blank=True)
    company_name = models.CharField(max_length=300, null=True, blank=True)
    city = models.CharField(max_length=200, null=True, blank=True)
    experience = models.CharField(max_length=300, null=True, blank=True)
    schedule = models.CharField(max_length=400, null=True, blank=True)
    job_description = models.TextField()
    published_date = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.position


